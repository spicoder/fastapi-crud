from pydantic import BaseModel

# Create todo schema (Pydantic Model)
class TodoCreate(BaseModel):
    task: str

class Todo(BaseModel):
    id: int
    task: str
    
    class Config:
        orm_mode = True