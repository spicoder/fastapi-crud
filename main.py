from typing import List
from fastapi import FastAPI, status, Depends, HTTPException
from database import Base, engine, SessionLocal
from sqlalchemy.orm import Session
import models
import schemas

# Create the database
Base.metadata.create_all(engine)

# Initialize app
app = FastAPI()


# Helper function to get database session
def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@app.get("/")
def root():
    return "todo"


@app.get("/todo", response_model=List[schemas.Todo])
def read_todo_list(session: Session = Depends(get_session)):
    # get all todo items
    todo_list = session.query(models.Todo).all()

    return todo_list


@app.get("/todo/{id}", response_model=schemas.Todo)
def read_todo(id: int, session: Session = Depends(get_session)):
    # get item with the given id
    todo = session.query(models.Todo).get(id)

    # check if id exists. If not, return 404 not found response
    if not todo:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found")

    return todo


@app.post("/todo", response_model=schemas.Todo, status_code=status.HTTP_201_CREATED)
def create_todo(todo: schemas.TodoCreate, session: Session = Depends(get_session)):
    tododb = models.Todo(task=todo.task)

    session.add(tododb)
    session.commit()
    session.refresh(tododb)

    return tododb


@app.put("/todo/{id}", response_model=schemas.Todo)
def update_todo(id: int, task: str, session: Session = Depends(get_session)):
    todo = session.query(models.Todo).get(id)

    if todo:
        todo.task = task
        session.commit()

    # check if id exists. If not, return 404 not found response
    if not todo:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found")

    return todo


@app.delete("/todo/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_todo(id: int, session: Session = Depends(get_session)):
    # get the given id
    todo = session.query(models.Todo).get(id)

    # if todo item with given id exists, delete it from db otherwise raise 404 error
    if todo:
        session.delete(todo)
        session.commit()
    else:
        raise HTTPException(status_code=404, detail=f"todo item with id {id} not found")

    return None
