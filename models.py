from sqlalchemy import Column, Integer, String
from database import Base

# Define Todo class from Base
class Todo(Base):
    __tablename__='todos'
    id = Column(Integer, primary_key=True)
    task = Column(String(256))
    